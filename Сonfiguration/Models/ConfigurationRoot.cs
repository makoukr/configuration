﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Сonfiguration.Interfaces;

namespace Сonfiguration
{
    /// <summary>
    /// The root node for a configuration.
    /// </summary>
    public class ConfigurationRoot : IConfigurationRoot
    {
        /// <summary>
        /// Initializes a Configuration root with a list of providers.
        /// </summary>
        /// <param name="providers">The <see cref="IConfigurationProvider"/>s for this configuration.</param>
        public ConfigurationRoot(IConfigurationProvider provider)
        {
            if (provider == null)
                throw new ArgumentNullException(nameof(provider));

            Provider = provider;
        }

        /// <summary>
        /// The <see cref="IConfigurationProvider"/>s for this configuration.
        /// </summary>
        public IConfigurationProvider Provider { get; }

        /// <summary>
        /// Gets or sets the value corresponding to a configuration key.
        /// </summary>
        /// <param name="key">The configuration key.</param>
        /// <returns>The configuration value.</returns>
        public string this[string key]
        {
            get => Provider.TryGet(key, out var value) ? value : null;
            set => Provider.Set(key, value);
        }

        public bool ContainsSection(string sectionKey)
        {
            if (Provider == null || Provider.Data == null || !Provider.Data.Any())
                return false;

            return Provider.Data.Keys.Any(key => key.StartsWith(sectionKey));
        }

        /// <summary>
        /// Gets the immediate children sub-sections.
        /// </summary>
        /// <returns>The children.</returns>
        public IEnumerable<IConfigurationSection> GetChildren() => Provider.Data.Keys.Select(key => GetSection(key));

        /// <summary>
        /// Gets a configuration sub-section with the specified key.
        /// </summary>
        /// <param name="key">The key of the configuration section.</param>
        /// <returns>The <see cref="IConfigurationSection"/>.</returns>
        /// <remarks>
        ///     This method will never return <c>null</c>. If no matching sub-section is found with the specified key,
        ///     an empty <see cref="IConfigurationSection"/> will be returned.
        /// </remarks>
        public IConfigurationSection GetSection(string key) => new ConfigurationSection(this, key);

        /// <summary>
        /// Loads configuration values.
        /// </summary>
        public async Task LoadAsync() => await Provider.LoadAsync().ConfigureAwait(false);
    }
}
