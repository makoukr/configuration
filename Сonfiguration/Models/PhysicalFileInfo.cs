﻿using System;
using System.IO;
using Сonfiguration.Interfaces;

namespace Сonfiguration.Models
{
    public class PhysicalFileInfo : IFileInfo
    {
        private FileInfo _fileInfo;

        public PhysicalFileInfo(string filePath)
        {
            _fileInfo = new FileInfo(filePath);
            PhysicalPath = filePath;
        }

        public bool Exists => _fileInfo?.Exists ?? false;

        public long Length => _fileInfo?.Length ?? -1;

        public string PhysicalPath { get; }

        public string Name => _fileInfo?.Name ?? string.Empty;

        public DateTimeOffset LastModified => _fileInfo?.LastWriteTime ?? DateTime.Now;

        public bool IsDirectory => false;

        public StreamReader CreateStreamReader() => new StreamReader(PhysicalPath);
    }
}
