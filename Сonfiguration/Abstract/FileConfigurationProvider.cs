﻿using System;
using System.IO;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading.Tasks;

namespace Сonfiguration.Abstract
{
    /// <summary>
    /// Base class for file based <see cref="ConfigurationProvider"/>.
    /// </summary>
    public abstract class FileConfigurationProvider : ConfigurationProvider
    {
        /// <summary>
        /// Initializes a new instance with the specified source.
        /// </summary>
        /// <param name="source">The source settings.</param>
        public FileConfigurationProvider(FileConfigurationSource source)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }
            Source = source;
        }

        /// <summary>
        /// The source settings for this provider.
        /// </summary>
        public FileConfigurationSource Source { get; }

        /// <summary>
        /// Generates a string representing this provider name and relevant details.
        /// </summary>
        /// <returns> The configuration name. </returns>
        public override string ToString()
            => $"{GetType().Name} for '{Source.Path}'";

        /// <summary>
        /// Loads the contents of the file at <see cref="Path"/>.
        /// </summary>
        /// <exception cref="FileNotFoundException">If Optional is <c>false</c> on the source and a
        /// file does not exist at specified Path.</exception>
        public override async Task LoadAsync()
        {
            var file = Source.FileInfo;

            if (file == null || !file.Exists)
            {
                var error = new StringBuilder($"The configuration file '{Source.Path}' was not found and is not optional.");
                if (!string.IsNullOrEmpty(file?.PhysicalPath))
                {
                    error.Append($" The physical path is '{file.PhysicalPath}'.");
                }
                HandleException(ExceptionDispatchInfo.Capture(new FileNotFoundException(error.ToString())));
                return;
            }

            using (var streamReader = file.CreateStreamReader())
            {
                try
                {
                    await LoadAsync(streamReader).ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    HandleException(ExceptionDispatchInfo.Capture(e));
                }
            }
        }

        /// <summary>
        /// Loads this provider's data from a stream.
        /// </summary>
        /// <param name="stream">The stream to read.</param>
        public abstract Task LoadAsync(StreamReader streamReader);

        private void HandleException(ExceptionDispatchInfo info)
        {
            bool ignoreException = false;
            if (Source.OnLoadException != null)
            {
                var exceptionContext = new FileLoadExceptionContext
                {
                    Provider = this,
                    Exception = info.SourceException
                };
                Source.OnLoadException.Invoke(exceptionContext);
                ignoreException = exceptionContext.Ignore;
            }
            if (!ignoreException)
            {
                info.Throw();
            }
        }
    }
}
