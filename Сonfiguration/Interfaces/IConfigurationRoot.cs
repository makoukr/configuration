﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Сonfiguration.Interfaces
{
    /// <summary>
    /// Represents the root of an <see cref="IConfiguration"/> hierarchy.
    /// </summary>
    public interface IConfigurationRoot : IConfiguration
    {
        /// <summary>
        /// Loads configuration values from the underlying <see cref="IConfigurationProvider"/>s.
        /// </summary>
        Task LoadAsync();

        bool ContainsSection(string sectionKey);

        /// <summary>
        /// The <see cref="IConfigurationProvider"/>s for this configuration.
        /// </summary>
        IConfigurationProvider Provider { get; }
    }
}
