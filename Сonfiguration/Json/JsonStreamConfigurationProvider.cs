﻿using System;
using System.IO;
using System.Threading.Tasks;
using Сonfiguration.Abstract;

namespace Сonfiguration.Json
{
    /// <summary>
    /// Loads configuration key/values from a json stream into a provider.
    /// </summary>
    public class JsonStreamConfigurationProvider : StreamConfigurationProvider
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="source">The <see cref="JsonStreamConfigurationSource"/>.</param>
        public JsonStreamConfigurationProvider(JsonStreamConfigurationSource source) : base(source) { }

        /// <summary>
        /// Loads json configuration key/values from a stream into a provider.
        /// </summary>
        /// <param name="streamReader">The json <see cref="StreamReader"/> to load configuration data from.</param>
        public override async Task LoadAsync(StreamReader streamReader)
        {
            Data = await JsonConfigurationFileParser.ParseAsync(streamReader);
        }
    }
}
