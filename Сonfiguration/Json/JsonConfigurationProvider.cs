﻿using System;
using System.IO;
using System.Threading.Tasks;
using Сonfiguration.Abstract;

namespace Сonfiguration.Json
{
    /// <summary>
    /// A JSON file based <see cref="FileConfigurationProvider"/>.
    /// </summary>
    public class JsonConfigurationProvider : FileConfigurationProvider
    {
        /// <summary>
        /// Initializes a new instance with the specified source.
        /// </summary>
        /// <param name="source">The source settings.</param>
        public JsonConfigurationProvider(JsonConfigurationSource source) : base(source) { }

        /// <summary>
        /// Loads the JSON data from a stream.
        /// </summary>
        /// <param name="stream">The stream to read.</param>
        public override async Task LoadAsync(StreamReader streamReader)
        {
            try
            {
                Data = await JsonConfigurationFileParser.ParseAsync(streamReader);
            }
            catch (Exception e)
            {
                throw new FormatException("Could not parse the JSON file", e);
            }
        }
    }
}
